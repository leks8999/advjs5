class Card {
  constructor(title, text, user) {
    this.title = title;
    this.text = text;
    this.user = user;
    this.element = this.createCardElement();
  }

  createCardElement() {
    const card = document.createElement('div');
    card.classList.add('card');
    card.style.background = '#b6aca8'
    card.style.textAlign = 'center'


    const cardTitle = document.createElement('h2');
    cardTitle.textContent = this.title;
    card.append(cardTitle);

    const cardText = document.createElement('p');
    cardText.textContent = this.text;
    card.append(cardText);

    const userInfo = document.createElement('div');
    userInfo.classList.add('user-info');

    const userFullName = document.createElement('h3');
    userFullName.textContent = this.user.name;
    userInfo.append(userFullName);

    const userEmail = document.createElement('p');
    userEmail.textContent = this.user.email;
    userInfo.append(userEmail);

    card.append(userInfo);

    const deleteBtn = document.createElement('button');
    deleteBtn.classList.add('btn');
    deleteBtn.textContent = 'delete post';

    deleteBtn.addEventListener('click', () => {
      fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, { method: 'DELETE' })
        .then(response => {
          if (response.ok) {
            this.element.remove();
          } else {
            throw new Error('Помилка видалення картки');
          }
        })
        .catch(error => {
          console.error(error);
        });
    });

    card.append(deleteBtn);

    return card;
  }
  setCardId(id) {
    this.id = id;
  }
}

fetch('https://ajax.test-danit.com/api/json/users')
  .then(response => response.json())
  .then(users => {
    return fetch('https://ajax.test-danit.com/api/json/posts')
      .then(response => response.json())
      .then(posts => {
        return { users, posts };
      });
  })
  .then(({ users, posts }) => {
    const body = document.body;
    posts.forEach(post => {
      const user = users.find(user => user.id === post.userId);
      const card = new Card(post.title, post.body, user);
      card.setCardId(post.id);
      body.prepend(card.element)
    })
  })








